package assignment101122;
//highest
import java.util.Scanner;

public class Q2v2 {

	//public static void main(String[] args) {

		int cse, ece, mech; 
		
		public static void main(String[] args) {
		
		Scanner sc = new Scanner(System.in); 
		
		System.out.println("Enter number of student for CSE");
		int cse = sc.nextInt(); 
		
		System.out.println("Enter number of student for ECE");
		int ece = sc.nextInt();
		
		System.out.println("Enter number of student for MECH");
		int mech = sc.nextInt();
		
		if ((cse >= 0) && (ece >= 0) && (mech >=0)) { //to clarify positive only
			if (cse > ece && cse > mech){
				System.out.println("Highest placement: "+cse+" "+"CSE");
				
			} else if (ece > cse && ece > mech) {
				System.out.println("Highest placement"+ece+" "+"ECE");
				
			} else if (mech > cse && mech > ece) {
				System.out.println("Highest placement"+mech+" "+"MECH");
				
			} else if (cse == 0 || ece == 0 || mech == 0) {
				System.out.println("None of the department has got the highest placement");
			}  else if ((cse == ece) && (cse == mech) && (ece == mech)){
				System.out.println("None of the department has got the highest placement");
			} else if ((cse == ece) && (cse > mech) && (ece > mech)) {
				System.out.println("Highest placement");
				System.out.println("CSE");
				System.out.println("ECE");
			} else if ((cse == mech) && (cse > ece) && (mech > ece)) {
			  	System.out.println("Highest placement");
			  	System.out.println("CSE");
			  	System.out.println("MECH");
			} else if ((ece == mech) && (ece > cse) && (mech > cse)) {
			  	System.out.println("Highest placement");
			  	System.out.println("ECE");
			  	System.out.println("MECH");
			}
		} else {
			System.out.println("Input is invalid");
		} 
		
	 }

	}

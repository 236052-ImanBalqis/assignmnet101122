package assignment101122;
//average age
import java.util.Scanner;

public class Question3 {
	//int static ages;
	public static double calculateAverage(int[] ages) {
		
		int length = ages.length, sum = 0;
		double average;
		
		for (int i=0; i<length; i++) {
			sum = sum+ages[i];
		}
		average = (double)sum/length;
		return average;
	}
	public static void main(String[] args) {
		Scanner s = new Scanner (System.in);
		System.out.println("Enter employee Count: ");
		int employee_count = s.nextInt();
		
		if(employee_count>=2) {
			int[]ages = new int[employee_count];
			int flag = 1;
		for(int i=0; i<employee_count;i++) {
			System.out.println("Enter age of employee"+(i+1)+":");
			int age = s.nextInt();
			if(age>= 28 && age<=40){
				ages[i]=age;
			//s.close();	
			}
			else {
				System.out.println("Invalid age entered!!");
				flag = 0;
				break;
			}
		}
		if(flag==1) {
			
			System.out.format("Average of ages is: %.2f", calculateAverage(ages));
			
		}
		}
		else {
			System.out.println("Please enter a valid employee count!!");
		}
		}
		
	
}
